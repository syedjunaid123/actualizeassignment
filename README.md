
---
# WireMock with RestAssured in Java

This project demonstrates how to use WireMock with RestAssured in a Java application for API testing. WireMock allows you to easily mock HTTP responses, making it useful for testing API integrations and handling various scenarios without relying on external services.

## Prerequisites

- Java Development Kit (JDK) installed on your system
- Maven installed on your system
- IDE (such as IntelliJ IDEA or Eclipse) for Java development

## Getting Started

To get started with this project, follow these steps:

1. Clone the repository to your local machine:

    ```bash
    git clone https://gitlab.com/syedjunaid123/actualizeassignment.git
    ```

2. Navigate to the project directory:

    ```bash
    cd assignment-actualize
    ```

3. Open the project in your preferred IDE.

4. Run the following Maven command to build the project and download dependencies:

    ```bash
    mvn clean install
    ```

5. Once the build is successful, you can run the tests using the following command:

    ```bash
    mvn clean test
    ```

## Project Structure

The project structure is organized as follows:

- `src/main/java/API_Automation/Base`: Contains the Java source code for API Utils.
- `src/test/java/ApiTests`: Contains the test classes.
- `src/main/java/API_Automation/MockApis/mock_apis`: Contains WireMock mappings and stubs.
- `src/main/java/API_Automation/MockApis/Endpoints`: Contains all the API Endpoints stores in properties file.
- `src/main/java/API_Automation/MockApis/Payloads`: Contains all the Request Payloads and Response Payloads in POJO Classes

## How to Use

1. **Creating WireMock Stubs**: Define your stub mappings in the `src/main/java/API_Automation/MockApis/mock_apis/source` directory. These mappings specify the request URL, method, and response to be returned by WireMock.

2. **Writing Test Cases**: Write your test cases using RestAssured in the `src/test/java/ApiTests` directory. Use the WireMock server URL to make requests to your API and assert the responses.

3. **Running Tests**: Execute the tests using Maven Or run the APIs using TESTNG. The tests will start the WireMock server, apply the defined stubs, and validate the API behavior.

---

## Reporting

This project integrates Extent Reports for comprehensive test reporting and automatically stores generated artifacts based timestamped for easy reference.
Once the test execution is completed, the artifact can be downloaded from `src/test/resources/ExtentReport` directory.
---

## CICD Pipeline

CI/CD pipeline has been implemented for this project. After each build, the pipeline executes on GitLab. 

Pipeline configurations are stored in the `.gitlab-ci.yml` file. 

Following successful build completion on GitLab, artifacts can be downloaded, including the Extent report for analysis.
Here is the url where you can access the Run Status and artifacts ``https://gitlab.com/syedjunaid123/actualizeassignment/-/pipelines``

---
