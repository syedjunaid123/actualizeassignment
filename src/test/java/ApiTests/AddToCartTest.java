package ApiTests;

import API_Automation.Base.APIUtils;
import API_Automation.Base.Report;
import API_Automation.MockApis.Payloads.Response.CartResponse.CartFetchResponse;
import API_Automation.MockApis.Payloads.Response.CartResponse.CartInvalidResponse;
import API_Automation.MockApis.Payloads.Response.CartResponse.CartValidResponse;
import API_Automation.MockApis.Payloads.Response.LoginUserResponse.LoginUserInvalidResponse;
import com.aventstack.extentreports.ExtentTest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AddToCartTest extends APIUtils{
    String TestName = this.getClass().getSimpleName();
    ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void test_addToCartBooks() throws JsonProcessingException {
        ExtentTest Step = Report.report(TestName);

        Step.info("Step1: Execute the API");
        Response response = sendPostRequest(BASE_URL + rp.readProp("cart_success"), null, null, null);

        Step.info("Step2: Validate if the response is 200 OK");
        assertThatStatusIs(response, 200);

        Step.info("Step3: Validate if the response payload");
        CartValidResponse cartValidResponse = objectMapper.readValue(response.getBody().asString(), CartValidResponse.class);
        Assert.assertEquals(cartValidResponse.getMessage(), "Book added to cart successfully");
    }

    @Test
    public void test_addToCartsWithPUT() throws JsonProcessingException {
        ExtentTest Step = Report.report(TestName);

        Step.info("Step1: Execute the API");
        Response response = sendPutRequest(BASE_URL + rp.readProp("put_cart"), null, null, null);

        Step.info("Step2: Validate if the response is 405 Method Not Allowed");
        assertThatStatusIs(response, 405);

        Step.info("Step3: Validate if the response payload");
        CartInvalidResponse cartInvalidResponse = objectMapper.readValue(response.getBody().asString(), CartInvalidResponse.class);
        Assert.assertEquals(cartInvalidResponse.getError(), "Method Not Allowed.");
    }

    @Test
    public void test_addToCartWithPATCH() throws JsonProcessingException {
        ExtentTest Step = Report.report(TestName);

        Step.info("Step1: Execute the API");
        Response response = sendPatchRequest(BASE_URL + rp.readProp("patch_cart"), null, null, null);

        Step.info("Step2: Validate if the response is 405 Method Not Allowed");
        assertThatStatusIs(response, 405);

        Step.info("Step3: Validate if the response payload");
        CartInvalidResponse cartInvalidResponse = objectMapper.readValue(response.getBody().asString(), CartInvalidResponse.class);
        Assert.assertEquals(cartInvalidResponse.getError(), "Method Not Allowed");
    }

    @Test
    public void test_addToCartWithGET() throws JsonProcessingException {
        ExtentTest Step = Report.report(TestName);

        Step.info("Step1: Execute the API");
        Response response = sendGetRequest(BASE_URL + rp.readProp("get_cart"), null, null, null);

        Step.info("Step2: Validate if the response is 200  OK");
        assertThatStatusIs(response, 200);

        Step.info("Step3: Validate if the response payload");
        CartFetchResponse cartFetchResponse = objectMapper.readValue(response.getBody().asString(), CartFetchResponse.class);
        JsonNode responseJson = objectMapper.readTree(response.getBody().asString());
        if (!responseJson.isEmpty()) {
            System.out.println("JSON is not empty.");
        } else {
            System.out.println("JSON is empty.");
        }
    }

    @Test
    public void test_addToCartBooksNotFound() throws JsonProcessingException {
        ExtentTest Step = Report.report(TestName);

        Step.info("Step1: Execute the API");
        Response response = sendPostRequest(BASE_URL + rp.readProp("cart_invalid"), null, null, null);

        Step.info("Step2: Validate if the response is 404 Not Found");
        assertThatStatusIs(response, 404);

        Step.info("Step3: Validate if the response payload");
        CartInvalidResponse cartInvalidResponse = objectMapper.readValue(response.getBody().asString(), CartInvalidResponse.class);
        Assert.assertEquals(cartInvalidResponse.getError(), "Book unavailable to purchase");
    }
}