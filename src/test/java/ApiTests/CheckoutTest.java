package ApiTests;

import API_Automation.Base.APIUtils;
import API_Automation.Base.Report;
import API_Automation.MockApis.Payloads.Response.CartResponse.CartInvalidResponse;
import API_Automation.MockApis.Payloads.Response.Checkout.CheckoutInvalidResponse;
import API_Automation.MockApis.Payloads.Response.Checkout.CheckoutSuccessResponse;
import com.aventstack.extentreports.ExtentTest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CheckoutTest extends APIUtils{
    String TestName = this.getClass().getSimpleName();
    ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void test_checkoutBooks() throws JsonProcessingException {
        ExtentTest Step = Report.report(TestName);

        Step.info("Step1: Execute the API");
        Response response = sendPostRequest(BASE_URL + rp.readProp("checkout_success"), null, null, null);

        Step.info("Step2: Validate if the response is 200 OK");
        assertThatStatusIs(response, 200);

        Step.info("Step3: Validate if the response payload");
        CheckoutSuccessResponse checkoutSuccessResponse = objectMapper.readValue(response.getBody().asString(), CheckoutSuccessResponse.class);
        Assert.assertEquals(checkoutSuccessResponse.getMessage(), "Checkout successful");
    }

    @Test
    public void test_checkoutBooksInvalid() throws JsonProcessingException {
        ExtentTest Step = Report.report(TestName);

        Step.info("Step1: Execute the API");
        Response response = sendPostRequest(BASE_URL + rp.readProp("checkout_invalid"), null, null, null);

        Step.info("Step2: Validate if the response is 400 Method Not Allowed");
        assertThatStatusIs(response, 400);

        Step.info("Step3: Validate if the response payload");
        CheckoutInvalidResponse checkoutInvalidResponse = objectMapper.readValue(response.getBody().asString(), CheckoutInvalidResponse.class);
        Assert.assertEquals(checkoutInvalidResponse.getError(), "Missing items in the request");
    }

    @Test
    public void test_checkoutWithPUT() throws JsonProcessingException {
        ExtentTest Step = Report.report(TestName);

        Step.info("Step1: Execute the API");
        Response response = sendPutRequest(BASE_URL + rp.readProp("put_checkout"), null, null, null);

        Step.info("Step2: Validate if the response is 405 Method Not Allowed");
        assertThatStatusIs(response, 405);

        Step.info("Step3: Validate if the response payload");
        CheckoutInvalidResponse checkoutInvalidResponse = objectMapper.readValue(response.getBody().asString(), CheckoutInvalidResponse.class);
        Assert.assertEquals(checkoutInvalidResponse.getError(), "Method Not Allowed.");
    }

    @Test
    public void test_checkoutWithPATCH() throws JsonProcessingException {
        ExtentTest Step = Report.report(TestName);

        Step.info("Step1: Execute the API");
        Response response = sendPatchRequest(BASE_URL + rp.readProp("patch_checkout"), null, null, null);

        Step.info("Step2: Validate if the response is 405 Method Not Allowed");
        assertThatStatusIs(response, 405);

        Step.info("Step3: Validate if the response payload");
        CheckoutInvalidResponse checkoutInvalidResponse = objectMapper.readValue(response.getBody().asString(), CheckoutInvalidResponse.class);
        Assert.assertEquals(checkoutInvalidResponse.getError(), "Method Not Allowed");
    }

    @Test
    public void test_checkoutWithGET() throws JsonProcessingException {
        ExtentTest Step = Report.report(TestName);

        Step.info("Step1: Execute the API");
        Response response = sendGetRequest(BASE_URL + rp.readProp("get_checkout"), null, null, null);

        Step.info("Step2: Validate if the response is 405 Method Not Allowed");
        assertThatStatusIs(response, 405);

        Step.info("Step3: Validate if the response payload");
        CheckoutInvalidResponse checkoutInvalidResponse = objectMapper.readValue(response.getBody().asString(), CheckoutInvalidResponse.class);
        Assert.assertEquals(checkoutInvalidResponse.getError(), "Method Not Allowed");
    }
}