package ApiTests;

import API_Automation.Base.APIUtils;
import API_Automation.Base.Report;
import API_Automation.MockApis.Payloads.Request.RegisterUserRequest.Address;
import API_Automation.MockApis.Payloads.Request.RegisterUserRequest.UserRegistrationRequest;
import API_Automation.MockApis.Payloads.Response.RegisterUserResponse.RegisterUserInvalidResponse;
import API_Automation.MockApis.Payloads.Response.RegisterUserResponse.RegisterUserSuccessResponse;
import com.aventstack.extentreports.ExtentTest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;


public class RegistrationUserTest extends APIUtils{
    String TestName = this.getClass().getSimpleName();
    ObjectMapper objectMapper = new ObjectMapper();
    ExtentTest Step = Report.report(TestName);

//    UserRegistrationRequest registrationRequest;
//    Address address;

    @Test
    public void test_RegisteringUserSuccess() throws JsonProcessingException {


        Step.info("Step1: Execute the API");
        Object requestPayload = UserRegistrationRequest.builder()
                .username("actualize123")
                .email("user@autualize.com")
                .password("actualize123")
                .confirmPassword("actualize123")
                .firstName("Ali")
                .lastName("Zak")
                .address(Address.builder()
                        .street("Corniche")
                        .city("MBZ City")
                        .state("Abu Dhabi")
                        .zipCode("12345")
                        .country("UAE")
                        .build())
                .build();
        Response response = sendPostRequest(BASE_URL + rp.readProp("register_success"), requestPayload, null, null);

        Step.info("Step2: Validate if the response is 200 OK");
        assertThatStatusIs(response, 201);

        Step.info("Step3: Validate if the response payload");
        RegisterUserSuccessResponse registrationResponse = objectMapper.readValue(response.getBody().asString(), RegisterUserSuccessResponse.class);
        Assert.assertEquals(registrationResponse.getMessage(), "User registered successfully");
    }

    @Test
    public void test_RegisteringUserNoUsername() throws JsonProcessingException {

        Step.info("Step1: Execute the API");
        Object requestPayload = UserRegistrationRequest.builder()
                .email("user@autualize.com")
                .password("actualize123")
                .confirmPassword("actualize123")
                .firstName("Ali")
                .lastName("Zak")
                .address(Address.builder()
                        .street("Corniche")
                        .city("MBZ City")
                        .state("Abu Dhabi")
                        .zipCode("12345")
                        .country("UAE")
                        .build())
                .build();

        Response response = sendPostRequest(BASE_URL + rp.readProp("invalid_username_register"), requestPayload, null, null);

        Step.info("Step2: Validate if the response is 200 OK");
        assertThatStatusIs(response, 400);

        Step.info("Step3: Validate if the response payload");
        RegisterUserInvalidResponse response_payload = objectMapper.readValue(response.getBody().asString(), RegisterUserInvalidResponse.class);
        System.out.println("This is the code: "+response.getBody().asString());
        System.out.println("This is the response: "+ response_payload);

        Assert.assertEquals(response_payload.getError(), "Username is missing");
    }

    @Test
    public void test_RegisteringUserNoPassword() throws JsonProcessingException {

        Step.info("Step1: Execute the API");
        Object requestPayload = UserRegistrationRequest.builder()
                .username("actualize123")
                .email("user@autualize.com")
                .confirmPassword("actualize123")
                .firstName("Ali")
                .lastName("Zak")
                .address(Address.builder()
                        .street("Corniche")
                        .city("MBZ City")
                        .state("Abu Dhabi")
                        .zipCode("12345")
                        .country("UAE")
                        .build())
                .build();

        Response response = sendPostRequest(BASE_URL + rp.readProp("invalid_password_register"), requestPayload, null, null);

        Step.info("Step2: Validate if the response is 400 Bad Request");
        assertThatStatusIs(response, 400);

        Step.info("Step3: Validate if the response payload");
        RegisterUserInvalidResponse registrationResponse = objectMapper.readValue(response.getBody().asString(), RegisterUserInvalidResponse.class);
        Assert.assertEquals(registrationResponse.getError(), "Password is missing");
    }

    @Test
    public void test_RegisteringUserNoEmail() throws JsonProcessingException {

        Step.info("Step1: Execute the API");
        Object requestPayload = UserRegistrationRequest.builder()
                .username("actualize123")
                .password("actualize123")
                .confirmPassword("actualize123")
                .firstName("Ali")
                .lastName("Zak")
                .address(Address.builder()
                        .street("Corniche")
                        .city("MBZ City")
                        .state("Abu Dhabi")
                        .zipCode("12345")
                        .country("UAE")
                        .build())
                .build();

        Response response = sendPostRequest(BASE_URL + rp.readProp("invalid_email_register"), requestPayload, null, null);

        Step.info("Step2: Validate if the response is 400 Bad Request");
        assertThatStatusIs(response, 400);

        Step.info("Step3: Validate if the response payload");
        RegisterUserInvalidResponse registrationResponse = objectMapper.readValue(response.getBody().asString(), RegisterUserInvalidResponse.class);
        Assert.assertEquals(registrationResponse.getError(), "Email is missing");
    }

    @Test
    public void registeringUserWithoutBody() throws JsonProcessingException {

        Step.info("Step1: Execute the API");
        Response response = sendPostRequest(BASE_URL + rp.readProp("register_badError"), null, null, null);

        Step.info("Step2: Validate if the response is 400 Bad Request");
        assertThatStatusIs(response, 400);

        Step.info("Step3: Validate if the response payload");
        RegisterUserInvalidResponse registrationResponse = objectMapper.readValue(response.getBody().asString(), RegisterUserInvalidResponse.class);
        Assert.assertEquals(registrationResponse.getError(), "Bad Request - Request Payload is missing.");
    }

    @Test
    public void registeringUserWithGET() throws JsonProcessingException {

        Step.info("Step1: Execute the API");
        Response response = sendGetRequest(BASE_URL + rp.readProp("get_register"), null, null, null);

        Step.info("Step2: Validate if the response is 405 Method Not Allowed");
        assertThatStatusIs(response, 405);

        Step.info("Step3: Validate if the response payload");
        RegisterUserInvalidResponse registrationResponse = objectMapper.readValue(response.getBody().asString(), RegisterUserInvalidResponse.class);
        Assert.assertEquals(registrationResponse.getError(), "Method Not Allowed");
    }

    @Test
    public void registeringUserWithPUT() throws JsonProcessingException {

        Step.info("Step1: Execute the API");
        Response response = sendPutRequest(BASE_URL + rp.readProp("put_register"), null, null, null);

        Step.info("Step2: Validate if the response is 405 Method Not Allowed");
        assertThatStatusIs(response, 405);

        Step.info("Step3: Validate if the response payload");
        RegisterUserInvalidResponse registrationResponse = objectMapper.readValue(response.getBody().asString(), RegisterUserInvalidResponse.class);
        Assert.assertEquals(registrationResponse.getError(), "Method Not Allowed.");
    }

    @Test
    public void registeringUserWithPATCH() throws JsonProcessingException {

        Step.info("Step1: Execute the API");
        Response response = sendPatchRequest(BASE_URL + rp.readProp("patch_register"), null, null, null);

        Step.info("Step2: Validate if the response is 405 Method Not Allowed");
        assertThatStatusIs(response, 405);

        Step.info("Step3: Validate if the response payload");
        RegisterUserInvalidResponse registrationResponse = objectMapper.readValue(response.getBody().asString(), RegisterUserInvalidResponse.class);
        Assert.assertEquals(registrationResponse.getError(), "Method Not Allowed");
    }
}