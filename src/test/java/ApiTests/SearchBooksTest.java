package ApiTests;

import API_Automation.Base.APIUtils;
import API_Automation.Base.Report;
import API_Automation.MockApis.Payloads.Response.SearchBooksResponse.LoginUserResponse.SearchBooksInvalidResponse;
import API_Automation.MockApis.Payloads.Response.SearchBooksResponse.LoginUserResponse.SearchBooksResponse;
import com.aventstack.extentreports.ExtentTest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;


public class SearchBooksTest extends APIUtils{
    String TestName = this.getClass().getSimpleName();
    ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void test_searchBooksAll() throws JsonProcessingException {
        ExtentTest Step = Report.report(TestName);

        Step.info("Step1: Execute the API");
        Response response = sendGetRequest(BASE_URL + rp.readProp("search_books_all"), null, null, null);

        Step.info("Step2: Validate if the response is 200 OK");
        assertThatStatusIs(response, 200);

        Step.info("Step3: Validate if the response payload");
        SearchBooksInvalidResponse registrationResponse = objectMapper.readValue(response.getBody().asString(), SearchBooksInvalidResponse.class);
        JsonNode responseJson = objectMapper.readTree(response.getBody().asString());
        if (!responseJson.isEmpty()) {
            System.out.println("JSON is not empty.");
        } else {
            System.out.println("JSON is empty.");
        }
    }

    @Test
    public void test_searchBooksWithPUT() throws JsonProcessingException {
        ExtentTest Step = Report.report(TestName);

        Step.info("Step1: Execute the API");
        Response response = sendPutRequest(BASE_URL + rp.readProp("put_search_books"), null, null, null);

        Step.info("Step2: Validate if the response is 405 Method Not Allowed");
        assertThatStatusIs(response, 405);

        Step.info("Step3: Validate if the response payload");
        SearchBooksInvalidResponse registrationResponse = objectMapper.readValue(response.getBody().asString(), SearchBooksInvalidResponse.class);
        Assert.assertEquals(registrationResponse.getError(), "Method Not Allowed.");
    }

    @Test
    public void test_searchBooksWithPATCH() throws JsonProcessingException {
        ExtentTest Step = Report.report(TestName);

        Step.info("Step1: Execute the API");
        Response response = sendPatchRequest(BASE_URL + rp.readProp("patch_search_books"), null, null, null);

        Step.info("Step2: Validate if the response is 405 Method Not Allowed");
        assertThatStatusIs(response, 405);

        Step.info("Step3: Validate if the response payload");
        SearchBooksInvalidResponse registrationResponse = objectMapper.readValue(response.getBody().asString(), SearchBooksInvalidResponse.class);
        Assert.assertEquals(registrationResponse.getError(), "Method Not Allowed");
    }

    @Test
    public void test_searchBooksWithPOST() throws JsonProcessingException {
        ExtentTest Step = Report.report(TestName);

        Step.info("Step1: Execute the API");
        Response response = sendPostRequest(BASE_URL + rp.readProp("post_search_books"), null, null, null);

        Step.info("Step2: Validate if the response is 405 Method Not Allowed");
        assertThatStatusIs(response, 405);

        Step.info("Step3: Validate if the response payload");
        SearchBooksInvalidResponse registrationResponse = objectMapper.readValue(response.getBody().asString(), SearchBooksInvalidResponse.class);
        Assert.assertEquals(registrationResponse.getError(), "Method Not Allowed");
    }

    @Test
    public void test_searchSingleBookValid() throws JsonProcessingException {
        ExtentTest Step = Report.report(TestName);

        Step.info("Step1: Execute the API");
        Response response = sendGetRequest(BASE_URL + rp.readProp("single_book_valid"), null, null, null);

        Step.info("Step2: Validate if the response is 200 OK");
        assertThatStatusIs(response, 200);

        Step.info("Step3: Validate if the response payload");
        SearchBooksInvalidResponse registrationResponse = objectMapper.readValue(response.getBody().asString(), SearchBooksInvalidResponse.class);
        JsonNode responseJson = objectMapper.readTree(response.getBody().asString());
        if (!responseJson.isEmpty()) {
            System.out.println("JSON is not empty.");
        } else {
            System.out.println("JSON is empty.");
        }
    }

    @Test
    public void test_searchSingleBookInvalid() throws JsonProcessingException {
        ExtentTest Step = Report.report(TestName);

        Step.info("Step1: Execute the API");
        Response response = sendGetRequest(BASE_URL + rp.readProp("single_book_invalid"), null, null, null);

        Step.info("Step2: Validate if the response is 404 Not Found");
        assertThatStatusIs(response, 404);

        Step.info("Step3: Validate if the response payload");
        SearchBooksInvalidResponse registrationResponse = objectMapper.readValue(response.getBody().asString(), SearchBooksInvalidResponse.class);
        Assert.assertEquals(registrationResponse.getError(), "Book Not Found");
    }
}