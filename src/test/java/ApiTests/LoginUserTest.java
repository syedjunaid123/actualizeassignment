package ApiTests;

import API_Automation.Base.APIUtils;
import API_Automation.Base.Report;
import API_Automation.MockApis.Payloads.Response.LoginUserResponse.LoginUserInvalidResponse;
import com.aventstack.extentreports.ExtentTest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginUserTest extends APIUtils{
    String TestName = this.getClass().getSimpleName();
    ObjectMapper objectMapper = new ObjectMapper();


    @Test
    public void test_loginUserValid() {
        ExtentTest Step = Report.report(TestName);

        Step.info("Step1: Execute the API");
        Response response = sendPostRequest(BASE_URL + rp.readProp("login_valid"), null, null, null);

        Step.info("Step2: Validate if the response is 200 OK");
        assertThatStatusIs(response, 200);
    }


    @Test
    public void test_loginUserWithoutBody() throws JsonProcessingException {
        ExtentTest Step = Report.report(TestName);

        Step.info("Step1: Execute the API");
        Response response = sendPostRequest(BASE_URL + rp.readProp("login_unauthorised"), null, null, null);

        Step.info("Step2: Validate if the response is 401 Unauthorised");
        assertThatStatusIs(response, 401);

        Step.info("Step3: Validate if the response payload");
        LoginUserInvalidResponse registrationResponse = objectMapper.readValue(response.getBody().asString(), LoginUserInvalidResponse.class);
        Assert.assertEquals(registrationResponse.getError(), "Invalid username or password");
    }


    @Test
    public void test_loginUserInvalid() throws JsonProcessingException {
        ExtentTest Step = Report.report(TestName);

        Step.info("Step1: Execute the API");
        Response response = sendPostRequest(BASE_URL + rp.readProp("login_invalid"), null, null, null);

        Step.info("Step2: Validate if the response is 400 Bad Request");
        assertThatStatusIs(response, 400);

        Step.info("Step3: Validate if the response payload");
        LoginUserInvalidResponse registrationResponse = objectMapper.readValue(response.getBody().asString(), LoginUserInvalidResponse.class);
        Assert.assertEquals(registrationResponse.getError(), "username or password missing");
    }


    @Test
    public void test_loginUserWithGET() throws JsonProcessingException {
        ExtentTest Step = Report.report(TestName);

        Step.info("Step1: Execute the API");
        Response response = sendGetRequest(BASE_URL + rp.readProp("get_login"), null, null, null);

        Step.info("Step2: Validate if the response is 400 Bad Request");
        assertThatStatusIs(response, 405);

        Step.info("Step3: Validate if the response payload");
        LoginUserInvalidResponse registrationResponse = objectMapper.readValue(response.getBody().asString(), LoginUserInvalidResponse.class);
        Assert.assertEquals(registrationResponse.getError(), "Method Not Allowed");
    }


    @Test
    public void test_loginUserWithPUT() throws JsonProcessingException {
        ExtentTest Step = Report.report(TestName);

        Step.info("Step1: Execute the API");
        Response response = sendPutRequest(BASE_URL + rp.readProp("put_login"), null, null, null);

        Step.info("Step2: Validate if the response is 400 Bad Request");
        assertThatStatusIs(response, 405);

        Step.info("Step3: Validate if the response payload");
        LoginUserInvalidResponse registrationResponse = objectMapper.readValue(response.getBody().asString(), LoginUserInvalidResponse.class);
        Assert.assertEquals(registrationResponse.getError(), "Method Not Allowed.");
    }


    @Test
    public void test_loginUserWithPATCH() throws JsonProcessingException {
        ExtentTest Step = Report.report(TestName);

        Step.info("Step1: Execute the API");
        Response response = sendPatchRequest(BASE_URL + rp.readProp("patch_login"), null, null, null);

        Step.info("Step2: Validate if the response is 400 Bad Request");
        assertThatStatusIs(response, 405);

        Step.info("Step3: Validate if the response payload");
        LoginUserInvalidResponse registrationResponse = objectMapper.readValue(response.getBody().asString(), LoginUserInvalidResponse.class);
        Assert.assertEquals(registrationResponse.getError(), "Method Not Allowed");
    }
}