package API_Automation.Base;

import API_Automation.MockApis.Endpoints.ReadEndpoints;
import API_Automation.MockApis.mock_apis.config.WireMockConfig;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import java.util.Map;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

public class APIUtils extends WireMockConfig {
    public static ReadEndpoints rp = new ReadEndpoints();
    static String CONTENT_TYPE_APPLICATION_JSON = "application/json";
    public static String BASE_URL = rp.readProp("baseurl");

    public Response sendGetRequest(String url, Map<String, String> headerParameters, Map<String, String> queryParameters, Map<String, String> pathParameters) {
        String apiUrl = url;
        RequestSpecification requestSpecification = RestAssured.given().baseUri(apiUrl).with().contentType(CONTENT_TYPE_APPLICATION_JSON);
        if (queryParameters != null) {
            requestSpecification.queryParams(queryParameters);
        }
        if (pathParameters != null) {
            requestSpecification.pathParams(pathParameters);
        }
        if (headerParameters != null) {
            for (Map.Entry<String, String> headerParameter : headerParameters.entrySet()) {
                requestSpecification.header(headerParameter.getKey(), headerParameter.getValue());
            }
        }
        Response response = requestSpecification.log().all().request(Method.GET, apiUrl);
        System.out.println("Response Code: " + response.statusCode() + "\n" + response.asString());
        return response;
    }


    public Response sendPostRequest(String url, Object requestBody, Map<String, String> headerParameters, Map<String, String> queryParameters) {
        String apiUrl = url;
        RequestSpecification requestSpecification;

        if (requestBody == null) {
            requestSpecification = RestAssured.given().with().contentType(CONTENT_TYPE_APPLICATION_JSON);
        } else {
            requestSpecification = RestAssured.given().body(requestBody.toString()).with().contentType(CONTENT_TYPE_APPLICATION_JSON);
        }
        if (queryParameters != null) {
            requestSpecification.queryParams(queryParameters);
        }
        if (headerParameters != null) {
            for (Map.Entry<String, String> headerParameter : headerParameters.entrySet()) {
                requestSpecification.header(headerParameter.getKey(), headerParameter.getValue());
            }
        }
        Response response = requestSpecification.when().log().all().post(apiUrl);
        System.out.println("Response Code: " + response.statusCode() + "\n" + response.asString());
        return response;
    }

    public Response sendPutRequest(String url, Object requestBody, Map<String, String> headerParameters, Map<String, String> queryParameters) {
        String apiUrl = url;
        RequestSpecification requestSpecification;

        if (requestBody == null) {
            requestSpecification = RestAssured.given().with().contentType(CONTENT_TYPE_APPLICATION_JSON);
        } else {
            requestSpecification = RestAssured.given().body(requestBody.toString()).with().contentType(CONTENT_TYPE_APPLICATION_JSON);
        }
        if (queryParameters != null) {
            requestSpecification.queryParams(queryParameters);
        }
        if (headerParameters != null) {
            for (Map.Entry<String, String> headerParameter : headerParameters.entrySet()) {
                requestSpecification.header(headerParameter.getKey(), headerParameter.getValue());
            }
        }
        Response response = requestSpecification.when().log().all().put(apiUrl);
        System.out.println("Response Code: " + response.statusCode() + "\n" + response.asString());
        return response;
    }


    public Response sendPatchRequest(String url, Object requestBody, Map<String, String> headerParameters, Map<String, String> queryParameters) {
        String apiUrl = url;
        RequestSpecification requestSpecification;

        if (requestBody == null) {
            requestSpecification = RestAssured.given().with().contentType(CONTENT_TYPE_APPLICATION_JSON);
        } else {
            requestSpecification = RestAssured.given().body(requestBody.toString()).with().contentType(CONTENT_TYPE_APPLICATION_JSON);
        }
        if (queryParameters != null) {
            requestSpecification.queryParams(queryParameters);
        }
        if (headerParameters != null) {
            for (Map.Entry<String, String> headerParameter : headerParameters.entrySet()) {
                requestSpecification.header(headerParameter.getKey(), headerParameter.getValue());
            }
        }
        Response response = requestSpecification.when().log().all().patch(apiUrl);
        System.out.println("Response Code: " + response.statusCode() + "\n" + response.asString());
        return response;
    }

    public void assertThatStatusCodeIsOK(Response response) {
        assertThatStatusCodeEquals(response, 200);
    }


    public void assertThatStatusCodeEquals(Response response, int expectedStatusCode) {
        assertThatResponseIsNotNull(response);
        Assert.assertEquals(response.getStatusCode(), expectedStatusCode, "The Status code of the Response is different than expected");
    }


    public void assertThatResponseIsNotNull(Response response) {
        Assert.assertNotNull(response, "The Response object is null");
    }


    public Object getNodeResponseValue(Response response, String parameterPath) {
        return (response.getBody().jsonPath().get(parameterPath)).toString();
    }

    public void assertThatResponseParameterNotNull(Response response, String parameterPath) {
        Assert.assertNotNull(response.getBody());
        Assert.assertNotNull(getNodeResponseValue(response, parameterPath));
    }


    public void assertThatResponseParameterEquals(Response response, String parameterPath, Object expectedParameterValue) {
        Assert.assertEquals(getNodeResponseValue(response, parameterPath), expectedParameterValue, String.format("The value of '%s' parameter is different than expected", parameterPath));
    }

    public void validateSchema(Response response, String schemaPath) {
        System.out.println("json schema path " + schemaPath);
        System.out.println(this.getClass().getResource("/").getPath());

        response.then().assertThat().body(matchesJsonSchemaInClasspath(schemaPath));
    }

    public void assertThatStatusIs(Response response, int expectedValue) {
        Assert.assertEquals(response.getStatusCode(), expectedValue, "The Status code of the Response is different than expected");
    }

    @BeforeSuite
    public void initializeMockServer() {
        startWireMockServer();
    }

    @AfterSuite
    public void tearDownMockServer() {
        stopWireMockServer();
    }
}
