package API_Automation.Base;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

import org.testng.annotations.AfterClass;
import org.testng.asserts.SoftAssert;
import java.util.logging.Logger;
import java.util.Date;


public class Report {
    public static SoftAssert softAssert;
    public static final Logger LOGGER = Logger.getLogger(APIUtils.class.getName());

    public static ExtentReports extent = new ExtentReports();

    public static String projectPath = System.getProperty("user.dir");
    public static String timestamp = String.valueOf(new Date().getTime());
    public static String reportPath = projectPath+"/src/test/resources/ExtentReport/Report"+timestamp+".html";


    public static ExtentTest report(String TestCase) {
        ExtentSparkReporter spark = new ExtentSparkReporter(reportPath);
        extent.attachReporter(spark);
        ExtentTest test = extent.createTest(TestCase);
        return test;
    }
}
