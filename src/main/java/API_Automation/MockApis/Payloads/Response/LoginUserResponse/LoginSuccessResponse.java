package API_Automation.MockApis.Payloads.Response.LoginUserResponse;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

@AllArgsConstructor
@Data
@Builder
@NonNull
public class LoginSuccessResponse {
    @JsonProperty("userId")
    private String userId;

    @JsonProperty("token")
    private String token;
}
