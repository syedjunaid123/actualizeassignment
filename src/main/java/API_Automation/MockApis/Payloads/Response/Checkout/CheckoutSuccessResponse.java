package API_Automation.MockApis.Payloads.Response.Checkout;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CheckoutSuccessResponse {
    private String message;
    private String orderId;
}

