package API_Automation.MockApis.Payloads.Response.CartResponse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CartFetchResponse {
    private String title;
    private String author;
    private int year;

}
