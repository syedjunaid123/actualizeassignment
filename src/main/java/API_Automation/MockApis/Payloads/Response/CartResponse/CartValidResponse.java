package API_Automation.MockApis.Payloads.Response.CartResponse;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CartValidResponse {
    private String message;
    private String cartId;
}
