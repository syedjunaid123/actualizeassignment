package API_Automation.MockApis.Payloads.Response.RegisterUserResponse;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
@NonNull
public class RegisterUserSuccessResponse {
    private String userId;
    private String message;
}
