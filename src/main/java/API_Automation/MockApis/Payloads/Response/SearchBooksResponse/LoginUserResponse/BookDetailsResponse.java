package API_Automation.MockApis.Payloads.Response.SearchBooksResponse.LoginUserResponse;

import lombok.*;

@AllArgsConstructor
@Data
@Builder
@NonNull
public class BookDetailsResponse {
    private String title;
    private String author;
    private int year;
}
