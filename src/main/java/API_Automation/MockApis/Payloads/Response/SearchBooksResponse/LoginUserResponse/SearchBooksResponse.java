package API_Automation.MockApis.Payloads.Response.SearchBooksResponse.LoginUserResponse;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@Data
@Builder
@NonNull
public class SearchBooksResponse {
    private List<BookDetailsResponse> results;
    private int totalResults;
}
