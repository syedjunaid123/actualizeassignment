package API_Automation.MockApis.mock_apis.config;

public class StubMappings {
    static void addStubMappings() {
        StubLoader.UserLoginMapping();
        StubLoader.UserRegistration();
        StubLoader.SearchBooksMapping();
        StubLoader.AddToCartMapping();
        StubLoader.CheckoutMapping();
    }
}
