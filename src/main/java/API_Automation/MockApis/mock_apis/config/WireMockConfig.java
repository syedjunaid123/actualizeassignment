package API_Automation.MockApis.mock_apis.config;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;

import static API_Automation.Base.Report.extent;
import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class WireMockConfig {

    public static final int WIREMOCK_PORT = 8110;

    public void startWireMockServer() {
        WireMockServer wireMockServer = new WireMockServer(WireMockConfiguration.options().port(WIREMOCK_PORT));
        wireMockServer.start();
        System.out.println("***** Mock Server has been Started *****");
        configureFor("localhost", wireMockServer.port());
        StubMappings.addStubMappings();
//        StubLoader.UserRegistration();
    }


    public static void stopWireMockServer() {
        WireMockServer wireMockServer = new WireMockServer(WireMockConfiguration.options().port(WIREMOCK_PORT));
        wireMockServer.stop();
        System.out.println("***** Mock Server has been Ended *****");
        extent.flush();
    }
}
