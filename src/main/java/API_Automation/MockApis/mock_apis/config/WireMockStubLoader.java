package API_Automation.MockApis.mock_apis.config;

import com.github.tomakehurst.wiremock.client.WireMock;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WireMockStubLoader {

    private static final Logger LOGGER = LoggerFactory.getLogger(WireMockStubLoader.class);

    public static void loadAndConfigureStubs(String jsonFilePath) {
        String jsonStubs = loadJsonStubs(jsonFilePath);
        if (jsonStubs != null) {
            configureWireMockMappings(jsonStubs);
        } else {
            LOGGER.error("Failed to load JSON stubs from file: {}", jsonFilePath);
        }
    }

    private static String loadJsonStubs(String jsonFilePath) {
        String jsonStubs = null;
        try {
            File file = new File(jsonFilePath);
            InputStream inputStream = new FileInputStream(file);
            if (inputStream != null) {
                jsonStubs = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
            } else {
                LOGGER.error("Error: Could not load JSON stubs from file path: {}", jsonFilePath);
            }
        } catch (IOException e) {
            LOGGER.error("Error reading JSON stubs from file: {}", e.getMessage(), e);
        }
        return jsonStubs;
    }

    private static void configureWireMockMappings(String jsonStubs) {
        JSONObject jsonObject = new JSONObject(jsonStubs);
        JSONArray mappings = jsonObject.getJSONArray("mappings");

        for (int i = 0; i < mappings.length(); i++) {
            JSONObject mapping = mappings.getJSONObject(i);
            JSONObject request = mapping.getJSONObject("request");
            JSONObject response = mapping.getJSONObject("response");

            String methodString = request.getString("method");
            String url = request.getString("url");

            WireMock.stubFor(WireMock.request(methodString, WireMock.urlEqualTo(url))
                    .withHeader("Content-Type", WireMock.equalTo("application/json"))
                    .willReturn(WireMock.aResponse()
                            .withStatus(response.getInt("status"))
                            .withHeader("Content-Type", response.getJSONObject("headers").getString("Content-Type"))
                            .withBody(response.getString("body"))));
        }
    }
}
