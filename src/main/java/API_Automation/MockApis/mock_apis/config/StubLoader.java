package API_Automation.MockApis.mock_apis.config;

public class StubLoader {

    private static final String StubsJSONPath = System.getProperty("user.dir") + "/src/main/java/API_Automation/MockApis/mock_apis/source/";

    public static void UserRegistration() {
        WireMockStubLoader.loadAndConfigureStubs(StubsJSONPath + "UserRegistrationStub.json");
    }

    public static void UserLoginMapping() {
        WireMockStubLoader.loadAndConfigureStubs(StubsJSONPath + "UserLoginStub.json");
    }

    public static void SearchBooksMapping() {
        WireMockStubLoader.loadAndConfigureStubs(StubsJSONPath + "SearchBooksStub.json");
    }

    public static void CheckoutMapping() {
        WireMockStubLoader.loadAndConfigureStubs(StubsJSONPath + "CheckoutStub.json");
    }

    public static void AddToCartMapping() {
        WireMockStubLoader.loadAndConfigureStubs(StubsJSONPath + "AddToCartStub.json");
    }
}
