# API Documentation: WireframeMocks

This document provides detailed information about the endpoints available in the WireframeMocks API.

## Base URL

The base URL for all endpoints is `http://localhost:8080`.

## Authentication

No authentication is required for accessing the endpoints.

---
## RegisterUser

### RegisterUser_Success

#### Endpoint: `/users/register/success`

- **Method**: POST
- **Description**: Registers a new user with valid information.
- **Request Body**:
  ```json
  {
      "username": "actualize123",
      "email": "user@autualize.com",
      "password": "actualize123",
      "confirmPassword": "actualize123",
      "firstName": "Ali",
      "lastName": "Zak",
      "address": {
          "street": "Corniche",
          "city": "MBZ City",
          "state": "Abu Dhabi",
          "zipCode": "12345",
          "country": "UAE"
      }
  }
  ```
- **Response**: No specific response provided.

### RegisterUser_Invalid

#### Endpoint: `/users/register/invalid`

- **Method**: POST
- **Description**: Registers a new user with invalid information.
- **Request Body**: Same as RegisterUser_Success
- **Response**: No specific response provided.

### RegisterUser_Invalid_UserName

#### Endpoint: `/users/register/invalid/username`

- **Method**: POST
- **Description**: Registers a new user with an invalid username.
- **Request Body**: Same as RegisterUser_Success
- **Response**: No specific response provided.

### RegisterUser_Invalid_Password

#### Endpoint: `/users/register/invalid/password`

- **Method**: POST
- **Description**: Registers a new user with an invalid password.
- **Request Body**: Same as RegisterUser_Success
- **Response**: No specific response provided.

### RegisterUser_Invalid_Email

#### Endpoint: `/users/register/invalid/email`

- **Method**: POST
- **Description**: Registers a new user with an invalid email.
- **Request Body**: Same as RegisterUser_Success
- **Response**: No specific response provided.

### getRegisterUser_Failure

#### Endpoint: `/get/users/register`

- **Method**: GET
- **Description**: Retrieves registration information but returns failure.
- **Request Body**: Empty
- **Response**: No specific response provided.

### putRegisterUser_Failure

#### Endpoint: `/put/users/register`

- **Method**: PUT
- **Description**: Updates registration information but returns failure.
- **Request Body**: Empty
- **Response**: No specific response provided.

### patchRegisterUser_Failure

#### Endpoint: `/patch/users/register`

- **Method**: PATCH
- **Description**: Partially updates registration information but returns failure.
- **Request Body**: Empty
- **Response**: No specific response provided.

---
## LoginUser

### LoginUser_Valid

#### Endpoint: `/users/login/valid`

- **Method**: POST
- **Description**: Logs in a user with valid credentials.
- **Request Body**:
  ```json
  {
      "username": "example_user",
      "password": "example_password"
  }
  ```
- **Response**: No specific response provided.

### LoginUser_Unauthorised

#### Endpoint: `/users/login/unauthorised`

- **Method**: POST
- **Description**: Logs in a user with invalid credentials.
- **Request Body**: Same as LoginUser_Valid
- **Response**: No specific response provided.

### LoginUser_Invalid

#### Endpoint: `/users/login/invalid`

- **Method**: POST
- **Description**: Logs in a user with invalid credentials.
- **Request Body**: Same as LoginUser_Valid
- **Response**: No specific response provided.

### getLoginUser_Failure

#### Endpoint: `/get/users/login`

- **Method**: GET
- **Description**: Retrieves login information but returns failure.
- **Request Body**: Empty
- **Response**: No specific response provided.

### putLoginUser_Failure

#### Endpoint: `/put/users/login`

- **Method**: PUT
- **Description**: Updates login information but returns failure.
- **Request Body**: Empty
- **Response**: No specific response provided.

### patchLoginUser_Failure

#### Endpoint: `/patch/users/login`

- **Method**: PATCH
- **Description**: Partially updates login information but returns failure.
- **Request Body**: Empty
- **Response**: No specific response provided.

---
## SearchBooks

### SearchAll_Books_Success

#### Endpoint: `/search/books/all`

- **Method**: GET
- **Description**: Retrieves all books successfully.
- **Request Body**: Empty
- **Response**: No specific response provided.

### SearchBooks_Single_Valid

#### Endpoint: `/search/books/valid?bookName=HarryPotter`

- **Method**: GET
- **Description**: Searches for a single book with a valid name.
- **Request Body**: Empty
- **Response**: No specific response provided.

### SearchBooks_Single_Invalid

#### Endpoint: `/search/books/invalid?bookName=HarryPotter`

- **Method**: GET
- **Description**: Searches for a single book with an invalid name.
- **Request Body**: Empty
- **Response**: No specific response provided.

### postSearchBooks_Failure

#### Endpoint: `/post/search/books`

- **Method**: POST
- **Description**: Posts search query for books but returns failure.
- **Request Body**: Empty
- **Response**: No specific response provided.

### putSearchBooks_Failure

#### Endpoint: `/put/search/books`

- **Method**: PUT
- **Description**: Updates search query for books but returns failure.
- **Request Body**: Empty
- **Response**: No specific response provided.

### patchSearchBooks_Failure

#### Endpoint: `/patch/search/books`

- **Method**: PATCH
- **Description**: Partially updates search query for books but returns failure.
- **Request Body**: Empty
- **Response**: No specific response provided.

---
## Checkout

### Checkout_Success

#### Endpoint: `/checkout/success`

- **Method**: POST
- **Description**: Processes checkout successfully.
- **Request Body**:
  ```json
  {
    "items": ["book1", "book2", "book3"],
    "shippingAddress": {
      "country": "USA",
      "city": "New York",
      "zipCode": "10001"
    }
  }
  ```
- **Response**: No specific response provided.

### Checkout_Unauthorised

#### Endpoint: `/checkout/invalid`

- **Method**: POST
- **Description**: Processes checkout but returns unauthorised.
- **Request Body**: Same as Checkout_Success
- **Response**: No specific response provided.

### getCheckout_Failure

#### Endpoint: `/get/checkout`

- **Method**: GET
- **Description**: Retrieves checkout information but returns failure.
- **Request Body**: Empty
- **Response**: No specific response provided.

### putCheckout_Failure

#### Endpoint: `/put/checkout`

- **Method**: PUT
- **Description**: Updates checkout information but returns failure.
- **Request Body**: Empty
- **Response**: No specific response provided.

### patchCheckout_Failure

#### Endpoint: `/patch/checkout`

- **Method**: PATCH
- **Description**: Partially updates checkout information but returns failure.
- **Request Body**: Empty
- **Response**: No specific

response provided.

---
### Conclusion


This documentation provides a comprehensive overview of the endpoints available in the WireframeMocks API, along with their descriptions, request bodies, and responses.